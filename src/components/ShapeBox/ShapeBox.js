import './ShapeBox.css';

const ShapeBox = ( props ) => {

  return (
    <div className='ShapeBox'>
      {props.children}
    </div>
  );
}

export default ShapeBox;
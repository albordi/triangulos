import { Stage, Layer, Line } from 'react-konva';

const Triangle = ({ A, B, C, color, dim }) => {

  return (
    <>
      <Stage width={dim} height={dim}>
        <Layer>
          <Line
            closed
            points={[A.x, A.y, B.x, B.y, C.x, C.y]}
            fill={color}
          />
        </Layer>
      </Stage>
    </>
  )
};

export default Triangle;
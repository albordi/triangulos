import { useEffect, useState } from 'react';
import ShapeBox from './components/ShapeBox/ShapeBox';
import Triangle from './components/Triangle/Triangle';
import './App.css';

function App() {

  const dim = 30;
  const [numTriangulos, setNumTriangulos] = useState(120);
  const [triangles, setTriangles] = useState([]);
  const [minX, setMinX] = useState(0);
  const [maxX, setMaxX] = useState(dim);
  const [minY, setMinY] = useState(0);
  const [maxY, setMaxY] = useState(dim);

  useEffect(() => {
    generateTrianglesPoints();
  }, []);

  function randomIntFromInterval(min, max) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

  const generateTrianglesPoints = () => {
    let newTriangles = [];
    for (let i = 0; i <= numTriangulos; i++) {
      // console.log(minX, maxX);
      // console.log(minY, maxY);

      let x1 = randomIntFromInterval(minX, maxX);
      let x2 = randomIntFromInterval(minX, maxX);
      let x3 = randomIntFromInterval(minX, maxX);
      let y1 = randomIntFromInterval(minY, maxY);
      let y2 = randomIntFromInterval(minY, maxY);
      let y3 = randomIntFromInterval(minY, maxY);

      //l
      let ab = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y1 - y2, 2));
      let ac = Math.sqrt(Math.pow(x1 - x3, 2) + Math.pow(y1 - y3, 2));
      let bc = Math.sqrt(Math.pow(x2 - x3, 2) + Math.pow(y2 - y3, 2));

      console.log('Lado 1', ab);
      console.log('Lado 2', ac);
      console.log('Lado 3', bc);


      let angulorad = Math.acos((-Math.pow(bc, 2) + Math.pow(ac, 2) + Math.pow(ab, 2)) / (2 * ac * ab));
      let a_ab_ac = (angulorad * 180) / Math.PI;

      angulorad = Math.acos((-Math.pow(ac, 2) + Math.pow(bc, 2) + Math.pow(ab, 2)) / (2 * bc * ab));
      let a_ab_bc = (angulorad * 180) / Math.PI;

      angulorad = Math.acos((-Math.pow(ab, 2) + Math.pow(ac, 2) + Math.pow(bc, 2)) / (2 * ac * bc));
      let a_ac_bc = (angulorad * 180) / Math.PI;

      let A = { x: x1, y: y1 }; let B = { x: x2, y: y2 }; let C = { x: x3, y: y3 };
      let newTriangle = { A, B, C, ab, ac, bc, a_ab_ac, a_ab_bc, a_ac_bc };
      newTriangles.push(newTriangle);
      setMinX(minX + dim);
      setMaxX(maxX + dim);
    }
    setTriangles(newTriangles);
  };

  return (
    <div className="App">
      {
        triangles.map((triangle) => {
          return (
            <ShapeBox>
              <Triangle A={triangle.A} B={triangle.B} C={triangle.C} color='green' dim={30} />
              <div className='text'>
                <p>A = {triangle.A.x}, {triangle.A.y}</p>
                <p>B = {triangle.B.x}, {triangle.B.y}</p>
                <p>C = {triangle.C.x}, {triangle.C.y}</p>
                <p>AB (l1) = {parseInt(triangle.ab)}</p>
                <p>AC (l2) = {parseInt(triangle.ac)}</p>
                <p>BC (l3) = {parseInt(triangle.bc)}</p>
                <p>a_ab_ac = {parseInt(triangle.a_ab_ac)}</p>
                <p>a_ab_bc = {parseInt(triangle.a_ab_bc)}</p>
                <p>a_ac_bc = {parseInt(triangle.a_ac_bc)}</p>

              </div>
            </ShapeBox>
          )
        })
      }
    </div>
  );
}

export default App;
